# nestjs-aws-sqs-transporter

Custom transporter for nestjs microservices built on top of AWS Simple Queue Service

## Description
This package is a lightweight implementation of Custom Transport Strategy and ClientProxy for AWS SQS.
It has been made to fulfill needs of an internal project and thus is not yet made to cover all aspect of Transport strategies as NestJS provide. (i.e MessagePattern is not supported).
This package is under development as the project relying on is growing.

## Usage
The package provide support for consuming multiple SQS queues (normal & FIFO) just by providing the queue name inside @EventPattern
```ts
@EventPattern('<queue-name>.fifo') // Here the framework will look for messages inside the <queue-name>.fifo queue
```

To Access your Message.Body use the @Payload decorator like this
```ts
async myHandler(@Payload() payload: MyPayloadType) {
```

You can also access the SQSContext containing the rest of AWS ReceiveMessage Information doing the following
```ts
async myHandler(@Payload() payload: MyPayloadType, @Ctx() ctx: SQSContext) {
  ctx.getQueueName() // will give you the value of the queue you are listening to
  ctx.getAwsDetails() // will give you fileds part of the ReceiveMessage listened by the server
```

To configure the transport layer you need to do the following
```ts
app.connectMicroservice<MicroserviceOptions>({
    strategy: new ServerSQS({
      sqsUri: 'AWS_SQS_QUEUE_URI', // https://sqs.eu-west-3.amazonaws.com/<account-id>
      region: 'AWS_SQS_REGION', // eu-west-3
      apiVersion: 'AWS_SQS_API_VERSION', // 2012-11-05
    })
})
```
There are other options described into the SQSOptions interface.

To configure the ClientProxy to emit to a SQS queue, you can do the following in the constructor of your controller/service
```ts
    this.sqsClient = new ClientSQS({
      sqsUri: 'AWS_SQS_QUEUE_URI',
      region: 'AWS_SQS_REGION',
      apiVersion: 'AWS_SQS_API_VERSION',
      perSendMessageUniqIdOptions: { // this option allows you to ask for uniq id compute for aws sqs parameters that you want
        MessageDeduplicationId: true, // if you want a uniq id for MessageDeduplicationId set this to true
        MessageGroupId: true, // if you want a uniq id for MessageGroupId set this to true
      },
      serializer: new OutboundMessageIdentitySerializer(), // The serializer provided by the package for the ClientProxy is mandatory
    })
```

After configuring your sqsClient, you can emit to a SQS Queue doing the following
```ts
    this.sqsClient.emit(queueName, payload) // simple way passing your payload. It will be available in Message.Body of the AWS SendMessage Message

    // If you want to add custom AWS parameters you can pass them on your payload and the serializer ill look for them to create corresponding aws parameters for the SendMessage
    // see the serializer for supported parameters
    this.sqsClient.emit(queueName, { ...payload, MessageAttributes: { '<your attribute>': { "DataType": "String", "StringValue": "<string value>" } } })
```

## Support
Feel free to open issues for improvement either bugfixing or new feature (such as @MessagePattern support)

## Roadmap
There is no clear roadmap since it has been develop for a project purpose that do not benefit for more utilities than what the package offers as of today.
But feel free to open issues for any needs
Unit tests will be available soon.

## Contributing
If you want to contribute to the package to help me solving issues, please email me at maxime.hourregue@gmail.com

## Authors and acknowledgment
This package has been made by mho96 <maxime.hourregue@gmail.com>

## License
This package is under a MIT License

## Project status
Under Development
