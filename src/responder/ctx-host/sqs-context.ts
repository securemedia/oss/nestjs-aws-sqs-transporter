import { BaseRpcContext } from '@nestjs/microservices/ctx-host/base-rpc.context';

type SQSContextArgs = any[];

export class SQSContext extends BaseRpcContext<SQSContextArgs> {
    constructor(args: SQSContextArgs) {
      super(args);
    }

    /**
     * @returns The name of the SQS queue.
     */
    getQueueName() {
      return this.args[0];
    }

    /**
     * 
     * @returns All AWS Details of ReceivedMessage 
     */
    getAwsDetails() {
      return this.args[1];
    }
}