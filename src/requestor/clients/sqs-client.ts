import { Logger } from '@nestjs/common';
import {
  ClientProxy,
  ReadPacket,
  WritePacket,
} from '@nestjs/microservices';

import { SQSOptions } from '../../interfaces/sqs-options.interface';

import * as AWS from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';

export class ClientSQS extends ClientProxy {
    protected readonly logger = new Logger(ClientProxy.name);
    protected sqsClient: AWS.SQS;
    protected connection: Promise<any>;

    constructor(protected readonly options?: SQSOptions) {
        super();
        this.initializeSerializer(options);
        this.initializeDeserializer(options);
    }

    // eslint-disable-next-line
    protected publish(partialPacket: ReadPacket, callback: (packet: WritePacket) => any): any {}

    protected dispatchEvent(packet: ReadPacket): Promise<any> {
        const pattern = this.normalizePattern(packet.pattern);
        const serializedPacket = this.serializer.serialize(packet);
        const { data, awsParameters } = serializedPacket
        const options = Object.keys(this.options.perSendMessageUniqIdOptions).reduce((acc, key) => {
            if (key) acc[key] = uuidv4()
            return acc
        }, {})
        const params = {
            MessageAttributes: { 'id': { "DataType": "String", "StringValue": uuidv4() }},
            MessageBody: JSON.stringify(data),
            QueueUrl: `${this.options.sqsUri}/${pattern}`,
            ...options,
            ...awsParameters
        };

        return this.sqsClient.sendMessage(params,
            (err, data) => err ? Promise.reject(err) : Promise.resolve(data)
        ).promise().catch(err => this.logger.log(err));
    }

    /**
     * connect -
     * instantiates AWS.SQS
     *
     * It returns the instance
     *
     * This construct is expected by the framework.
     */
    public async connect(): Promise<any> {
        if (this.sqsClient) {
            return this.sqsClient;
        }
        const { apiVersion, region } = this.options;
        if (!apiVersion) throw new Error('Unable to init AWS SQS, missing apiVersion')
        if (!region) throw new Error('Unable to init AWS SQS, missing region')
        AWS.config.update({ region })
        this.sqsClient = new AWS.SQS({ apiVersion });
        return this.sqsClient;
    }

    public close() {
        this.sqsClient = null;
    }
}