import { Logger } from '@nestjs/common';
import { ReadPacket, Serializer } from '@nestjs/microservices';
import { AWSReadPacket } from '../../interfaces/sqs-options.interface';


export class OutboundMessageIdentitySerializer implements Serializer {
    private readonly logger = new Logger('OutboundMessageIdentitySerializer');
    private readonly awsSQSParameters: string[] = ["MessageGroupId", "MessageAttributes"]
    serialize(value: ReadPacket): AWSReadPacket {
        const awsParameters = Object.keys(value.data).filter(key => this.awsSQSParameters.includes(key)).reduce((acc, key) => {
            acc[key] = value.data[key]
            delete value.data[key]
            return acc
        }, {})
        return { ...value, awsParameters };
    }
}
